import React from 'react'
import styled from 'styled-components'
import Layout from '../components/layout'
import Projects from '../components/projects'
import SEO from '../components/seo'
import Bio from '../components/bio'
import Articles from '../components/articles'

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <div className="container">
      <Bio />
      <Projects />
    </div>
    <Articles />
  </Layout>
)

export default IndexPage
