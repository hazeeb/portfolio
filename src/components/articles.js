import React from 'react'
import styled from 'styled-components'

const Articles = () => {
    return (
        <div style={{ backgroundColor: '#ededed' }}>
            <Main>
                <h1>Articles</h1>
                <Article>
                <li><a href="https://medium.com/@hazeeb.me/challenges-faced-while-developing-a-game-in-react-native-a02ae61921aa"><p>Challenges faced while developing a trivia game in react native.</p></a> </li>
                </Article>

                <Article>
                <a target="blank" href="https://medium.com/@hazeeb.me/use-socket-io-with-react-native-nov-2017-a115e346615a"><p>Use socket.io with react native</p></a> 
                </Article>

            </Main>

        </div>
    )
}

const Main = styled.div`
    margin: 5% 10% 0% 10%;
    padding: 50px 0;
    @media (max-width: 600px) {
    margin: 0;
    padding:10px;
    }
`
const Article = styled.li`
    list-style: none;
    font-size: 20px;
    @media (max-width: 600px) {
        padding:0 20px ;
      }
`
export default Articles