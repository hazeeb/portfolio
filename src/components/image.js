import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

const Image = (props) => {
  return (
    <StaticQuery
      query={graphql`
        query {
          placeholderImage: file(relativePath: { eq: "profile.jpeg" }) {
            childImageSharp {
              fluid(maxWidth: 200) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          pixx: file(relativePath: { eq: "pixx.png" }) {
            childImageSharp {
              fluid(maxWidth: 1200, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          picplayDash: file(relativePath: { eq: "picplayDash.jpg" }) {
            childImageSharp {
              fluid(maxWidth: 1200, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          challenger: file(relativePath: { eq: "challenger.jpg" }) {
            childImageSharp {
              fluid(maxWidth: 350, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          challenger2: file(relativePath: { eq: "challenger2.jpg" }) {
            childImageSharp {
              fluid(maxWidth: 350, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          challengerDash: file(relativePath: { eq: "challengerDash.png" }) {
            childImageSharp {
              fluid(maxWidth: 650, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          playtime: file(relativePath: { eq: "playtime.png" }) {
            childImageSharp {
              fluid(maxWidth: 350, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }

        }
      `}
      render={data => {
        const profileImage = data[props.name].childImageSharp.fluid
        return(
          <Img fluid={profileImage} style={props.style} />
        )
      }}
    />
  )
}
export default Image
