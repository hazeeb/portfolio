import { Link } from 'gatsby'
import React from 'react'
import styled from 'styled-components'
import Image from './image'

const Projects = () => {

  return(
    <div className="projects">
    <h1>Projects</h1>
    <Wrap>
      <Website>
      <Link to="/graphic-tool">
        <h2>Internal Graphic Tool</h2>
        <p className="subtitle">Online graphic design tool, easy drag and drop interface.</p>
        <Image name="pixx" className="web-img" /> </Link>
      </Website>
      <MobileApp>
        <Link to="/challenger">
          <h2>Challenger </h2>
          <p className="subtitle">Challenge your friends in realtime by playing trivia questions.</p>
          <Image name="challenger" style={{width: '250px', display: 'block', margin: 'auto'}}/>
          </Link>
      </MobileApp>
      <MobileApp>
          <h2>Playtime </h2>
          <p className="subtitle">Find and book nearby sport activities</p>
          <Image name="playtime" style={{width: '250px', display: 'block', margin: 'auto'}}/>
      </MobileApp>

    </Wrap>
  </div>
  )
}

const Wrap = styled.div`
    display:flex;
    justify-content: center;
    flex-flow: wrap;
`

const MobileApp = styled.div`
  border: 1px solid #ededed;
  margin: 10px;
  justify-content: center;
  text-align:center;
  flex:1 1 0;

  p{
    font-size: 20px;
  }
   
  @media only screen and (max-width: 600px) {
    width: 92%
  }
`
const Website = styled.div`
  border: 1px solid #ededed;
  margin: 10px;
  justify-content: center;
  text-align:center;
  width:98%
`

export default Projects
