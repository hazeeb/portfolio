import React from 'react'

const Footer = () => {
    return (
        <div style={{textAlign:'center', backgroundColor: '#fafafa', padding: '30px', fontWeight: '300', bottom: '0%',}}>
            <p>Let's get in touch. <a style={{textDecoration: 'none'}} href="https://www.linkedin.com/in/haseebp/">LinkedIn</a> messages
        and <a style={{textDecoration: 'none'}} href="mailto:hap12310@gmail.com">Email</a> works best.</p>
        </div>
    )
}

export default Footer